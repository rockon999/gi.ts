gi.ts
=====

`gi.ts` has moved to [gjsify/gi.ts](https://github.com/gjsify/gi.ts). The two (active) projects supporting TypeScript for GJS are merging 🥳